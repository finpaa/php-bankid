<?php

namespace Finpaa\Sweden;

use Da\QrCode\QrCode;
use Finpaa\Finpaa;

class BankID
{
    const STATUS_PENDING = "pending";
    const HINTCODE_OUTSTANDING_TRANSACTION = "outstandingTransaction";
    const HINTCODE_NO_CLIENT = "noClient";
    const MSG_OUSTANDING_TRANSACTION_OR_NO_CLIENT = "Starta BankID-appen";
    const HINTCODE_USER_SIGN = "userSign";
    const MSG_USER_SIGNED = "Skriv in din säkerhetskod i BankID- appen och välj Identifiera eller Skriv under.";
    const STATUS_FAILED = "failed";
    const HINTCODE_USER_CANCEL = "userCancel";
    const MSG_USER_CANCEL = "Åtgärden avbruten.";
    const HINTCODE_EXPIRED_TRANSACTION = "expiredTransaction";
    const MSG_EXPIRED_TRANSACTION = "BankID-appen svarar inte. Kontrollera att den är startad och att du har internetanslutning. Om du inte har något giltigt BankID kan du hämta ett hos din Bank. Försök sedan igen.";
    const HINTCODE_CERTIFICATE_ERROR = "certificateErr";
    const MSG_CERTIFICATE_ERROR = "Det BankID du försöker använda är för gammalt eller spärrat. Använd ett annat BankID eller hämta ett nytt hos din internetbank.";
    const HINTCODE_START_FAILED = "startFailed";
    const MSG_START_FAILED = "Misslyckades att läsa av QR koden. Starta BankID-appen och läs av QR koden. Kontrollera att BankID-appen är uppdaterad. Om du inte har BankID- appen måste du installera den och hämta ett BankID hos din internetbank. Installera appen från din appbutik eller https://install.bankid.com.";
    const STATUS_INVALID_PARAMETERS = "invalidParameters";
    const MSG_INVALID_PARAMETERS = "Tryck på QR-koden för att skanna igen.";
    const ERROR_CODE_CANCELLED = "cancelled";
    const MSG_ERROR_CODE_CANCELLED = "Åtgärden avbruten. Försök igen.";
    const ERROR_CODE_ALREADY_INPROGRESS = "alreadyInProgress";
    const MSG_Already_inprogress = "En identifiering eller underskrift för det här personnumret är redan påbörjad. Försök igen.";
    const ERROR_CODE_REQUEST_TIMEOUT = "requestTimeout";
    const ERROR_CODE_MAINTENANCE = "maintenance";
    const ERROR_CODE_INTERNAL_ERROR = "internalError";
    const MSG_BANKID_ERROR = "Internt tekniskt fel. Försök igen.";
    const MSG_UNKNOWN_ERROR = "Okänt fel. Försök igen.";
    const STATUS_COMPLETE = 'complete';

    // FINPAA
    private static $sequenceCode;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        self::$sequenceCode = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->BankID()->start();
    }

    private static function getSequenceCode()
    {
      if(self::$sequenceCode) {
        return self::$sequenceCode;
      }
      else {
        self::selfConstruct();
        return self::getSequenceCode();
      }
    }

    private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
    {
        $sequence = Finpaa::getSequenceMethods($code);

        if (isset($sequence->SequenceExecutions)) {

          if($methodIndex < count($sequence->SequenceExecutions))
          {
            $response = Finpaa::executeTheMethod(
              $sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
            );

            return $response;
          }
          else {
              return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
                'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions)
              );
          }
        }
        else {
           return array('error' => true, 'message' => $name .' -> No sequence executions', 'response' => $sequence);
       }
    }

    public static function getAuth($endUserIp, array $certs, $personalNumber = null, $dataToDisplay = null)
    {
        $alterations = array();
        $alterations[]['headers'] = $certs;
        $alterations[]['body'] = array(
          'endUserIp' => $endUserIp,
          'personalNumber' => $personalNumber
        );

        if($personalNumber)
          $alterations[]['headers']['Obey-Rules'] = "false";

        $response = self::executeSequenceMethod(
          self::getSequenceCode(), 0, $alterations, 'auth', false
        );

        $response->orderTime = time();
        return $response;
    }

    public static function getSign($endUserIp, array $certs, $personalNumber = null, $dataToDisplay = null)
    {
        $alterations = array();
        $alterations[]['headers'] = $certs;
        $alterations[]['body'] = array(
          'endUserIp' => $endUserIp,
          'personalNumber' => $personalNumber,
          'userVisibleData' => $dataToDisplay
        );

        if($personalNumber)
          $alterations[]['headers']['Obey-Rules'] = "false";

        $response = self::executeSequenceMethod(
          self::getSequenceCode(), 3, $alterations, 'sign', false
        );

        $response->orderTime = time();
        return $response;
    }

    public static function collect($data, array $certs)
    {
        $alterations = array();
        $alterations[]['headers'] = $certs;
        $alterations[]['body'] = array(
          'orderRef' => $data['orderRef'],
          'qrStartToken' => $data['qrStartToken'],
          'qrStartSecret' => $data['qrStartSecret'],
          'orderTime' => $data['orderTime'],
        );

        $response = self::executeSequenceMethod(
          self::getSequenceCode(), 1, $alterations, 'collect', false
        );
        return $response;
    }

    public static function getAnimatedQrCode($qrString)
    {
        $qrCode = (new QrCode($qrString))->setSize(250)->setMargin(5);
        return $qrCode->writeDataUri();
    }
}
